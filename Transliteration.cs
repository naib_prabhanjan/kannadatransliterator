﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;

namespace CSharpSolutions
{
    enum Language
    {
        kannaDa,
        samskruta,
        maLayáLam,
        brahmi,
        tamiL,
        telgu
    }
    class Transliteration
    {
        public string Language = String.Empty;
        public Dictionary<string, List<char>> Mappings = new Dictionary<string, List<char>>();

        List<string> sC2 = new List<string>
            {
                "kh", "gh", "nj",
                "ch", "CH", "jh", "ny",
                "Th", "Dh",
                "th", "dh",
                "ph", "bh",
                "sh", "SH", "lc"
            };

        List<string> sC1 = new List<string>
            {
                "k", "g",
                "j",
                "T", "D", "N",
                "t", "d", "n",
                "p", "b", "m",
                "y", "r", "l", "w", "s", "h", "L"
            };

        List<string> sV2 = new List<string>
            {
                "r_", "R_", "l_", "L_", "ai", "au", "am", "an", "ng", "a:"
            };

        string sVs = "rRlL";

        string sV1 = "aáiíuúeéoó.";
        string sNumbers = "0123456789";

        string sP1 = String.Empty;
        string sP2 = String.Empty;

        public Transliteration(Language Lang)
        {
            Language = Lang.ToString();
            string sLine = String.Empty;
            string[] sTemp;
            string sPath = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin"));
            using (StreamReader sr = new StreamReader($@"{sPath}\Mappings\{Lang.ToString()}Mappings.txt", Encoding.UTF32))
            {
                while ((sLine = sr.ReadLine()) != null)
                {
                    sTemp = sLine.Split(';');
                    Mappings.Add(sTemp[0], GetCharacterList(sTemp));
                }
            }
        }

        private List<char> GetCharacterList(string[] sList)
        {
            List<char> listOfChars = new List<char>();
            string sTemp = String.Empty;
            int p = 0;
            for (int i = 1; i < sList.Length; i++)
            {
                sTemp = sList[i];
                if (sTemp.Length > 1)
                {
                    p = int.Parse(sTemp, System.Globalization.NumberStyles.HexNumber);
                    listOfChars.Add((char)p);
                }
                else
                {
                    listOfChars.Add(Convert.ToChar(sTemp));
                }
            }
            return listOfChars;
        }
        public string TransliterateFromLatinChars(string s)
        {
            StringBuilder sb = new StringBuilder();
            int i = 0;
            while (i < s.Length)
            {
                sP1 = String.Empty;
                sP2 = String.Empty;
                // 2 letter consonants
                if ((i + 1) < s.Length && sC2.Contains($"{s[i]}{s[i + 1]}"))
                {
                    sP1 = Mappings[$"{s[i]}{s[i + 1]}a"][0].ToString();
                    if ((i + 3) < s.Length && sV2.Contains($"{s[i + 2]}{s[i + 3]}"))
                    {
                        sP2 = Mappings[$"x{s[i + 2]}{s[i + 3]}"][0].ToString();
                        i += 4;
                    }
                    else if ((i + 2) < s.Length && sV1.Contains(s[i + 2]))
                    {
                        if(s[i + 2] != 'a')
                        {
                            sP2 = Mappings[$"x{s[i + 2]}"][0].ToString();
                        }
                        i += 3;
                    }
                    else
                    {
                        sP2 = Mappings["x"][0].ToString();
                        i += 2;
                    }
                    sb.Append($"{sP1}{sP2}");
                }
                // 1 letter consonants
                else if (sC1.Contains($"{s[i]}"))
                {
                    if ((i + 1) < s.Length && sVs.Contains(s[i]) && s[i + 1] == '_')
                    {

                        sP1 = Mappings[$"{s[i]}_"][0].ToString();
                        sb.Append($"{sP1}");
                        i += 2;
                    }
                    else
                    {
                        sP1 = Mappings[$"{s[i]}a"][0].ToString();
                        if ((i + 2) < s.Length && sV2.Contains($"{s[i + 1]}{s[i + 2]}"))
                        {
                            sP2 = Mappings[$"x{s[i + 1]}{s[i + 2]}"][0].ToString();
                            i += 3;
                        }
                        else if ((i + 1) < s.Length && sV1.Contains(s[i + 1]))
                        {
                            if (s[i + 1] != 'a')
                            {
                                sP2 = Mappings[$"x{s[i + 1]}"][0].ToString();
                            }
                            i += 2;
                        }
                        else
                        {
                            sP2 = Mappings["x"][0].ToString();
                            i += 1;
                        }
                        sb.Append($"{sP1}{sP2}");
                    }
                }
                // 2 letter vowels
                else if ((i + 1) < s.Length && sV2.Contains($"{s[i]}{s[i + 1]}"))
                {
                    sP1 = $"{s[i]}{s[i + 1]}";
                    if (sP1 == "am" || sP1 == "an" || sP1 == "a:")
                    {
                        sP1 = Mappings[$"x{s[i]}{s[i + 1]}"][0].ToString();
                        sb.Append($"{Mappings["a"][0]}{sP1}");
                    }
                    else
                    {
                        sP1 = Mappings[$"{s[i]}{s[i + 1]}"][0].ToString();
                        sb.Append($"{sP1}");
                    }
                    i += 2;
                }
                // 1 letter vowels
                else if (sV1.Contains(s[i]))
                {
                    sP1 = Mappings[$"{s[i]}"][0].ToString();
                    sb.Append($"{sP1}");
                    i += 1;
                }
                // numbers
                else if (sNumbers.Contains(s[i]))
                {
                    sP1 = Mappings[$"{s[i]}"][0].ToString();
                    sb.Append($"{sP1}");
                    i += 1;
                }
                else
                {
                    if (s[i] != '\r')
                    {
                        sb.Append(s[i]);
                    }
                    else
                    {
                        sb.ToString();
                    }
                    i += 1;
                }
            }

            return sb.ToString();
        }
        private List<string> SortDescribing(MatchCollection mc)
        {
            List<string> tempList = new List<string>();
            foreach (Match m in mc)
            {
                tempList.Add(m.Value);
            }

            tempList = tempList.OrderByDescending(x => x.Length).ToList<string>();
            return tempList;
        }
    }
}
