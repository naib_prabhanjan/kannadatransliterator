﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CSharpSolutions
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Transliteration> listofTransliteration = new List<Transliteration>
            {
                new Transliteration(Language.kannaDa),
                new Transliteration(Language.telgu),
                new Transliteration(Language.samskruta),
                new Transliteration(Language.maLayáLam),
                new Transliteration(Language.brahmi),
                new Transliteration(Language.tamiL)
            };

            string sPath = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin"));
            string sText = String.Empty;

            // I use this website to generate random characters and with the following character set.
            // aáiíuúeéoó khgnjTDNtdpbm yrlwsL 0123456789
            // http://www.unit-conversion.info/texttools/random-string-generator/
            //
            using (StreamReader sr = new StreamReader($@"{sPath}\Input.txt", Encoding.UTF8))
            {
                sText = sr.ReadToEnd();
                sText = sText.TrimEnd('\r', '\n');
            }

            using (StreamWriter sw = new StreamWriter($@"{sPath}\output.txt", append: false, Encoding.UTF8))
            {
                sw.WriteLine($"kannaDa transliteration in Latin characters:");
                sw.WriteLine("--------------------------------");
                sw.WriteLine($"{sText}");
                sw.WriteLine("--------------------------------");

                string result = String.Empty;
                foreach(Transliteration t in listofTransliteration)
                {
                    result = t.TransliterateFromLatinChars(sText);
                    sw.WriteLine();
                    sw.WriteLine($"In {t.Language}");
                    sw.WriteLine("--------------------------------");
                    sw.WriteLine($"{result}");
                    sw.WriteLine("--------------------------------");
                }
            }
        }
    }
}
